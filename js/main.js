/* Открытие меню */
var main = function() { //главная функция
    $('.gamburger').click(function() { //выбираем класс icon-menu и добавляем метод click с функцией, вызываемой при клике
        $('.mobile-menu').animate({ //выбираем класс menu и метод animate
            left: '0px' //теперь при клике по иконке, меню, скрытое за левой границей на 285px, изменит свое положение на 0px и станет видимым
        }, 200); //скорость движения меню в мс

        $('body').animate({ //выбираем тег body и метод animate
            left: '285px' //чтобы всё содержимое также сдвигалось вправо при открытии меню, установим ему положение 285px
        }, 200); //скорость движения меню в мс
        $('body').addClass('overflow');
        $('body').css('overflow','hidden');
        $('.overlay').fadeIn();
    });

    /* Закрытие меню */
    $('.overlay').click(function() { //выбираем класс icon-close и метод click
        $('.mobile-menu').animate({ //выбираем класс menu и метод animate
            left: '-285px' //при клике на крестик меню вернется назад в свое положение и скроется
        }, 200); //скорость движения меню в мс

        $('body').animate({ //выбираем тег body и метод animate
            left: '0px' //а содержимое страницы снова вернется в положение 0px
        }, 200); //скорость движения меню в мс
        $('body').removeClass('overflow');
        $('body').css('overflow','visible');
        $('.overlay, .city-list-popup-window').fadeOut();
    });

    $('body').click(function(){
        if (!$('.search-new').hasClass("search-new-active")){
            $('.search-new').removeClass("search-new-active");
        }
    });

    $(".ic-search").click(function () {
        if ($(this).parent().hasClass("search-new-active")) {
            document.location.href = "/?s=" + $(this).parent().find("input").val();
        } else {
            $(this).parent().toggleClass("search-new-active");
        }
    });

    $('.city').click(function(s) {
        $('.city-list-popup-window, .overlay').fadeIn(300);
        $('body').css('overflow','hidden');
        $('.city-list-popup-window').css({'display':'flex'});
    });

    $('.close-window').click(function (q){
        $('.overlay').fadeOut();
        $('.city-list-popup-window').css({'display':'none'});
        $('body').css('overflow','visible');

        $('.mobile-menu').animate({ //выбираем класс menu и метод animate
            left: '-285px' //при клике на крестик меню вернется назад в свое положение и скроется
        }, 200); //скорость движения меню в мс

        $('body').animate({ //выбираем тег body и метод animate
            left: '0px' //а содержимое страницы снова вернется в положение 0px
        }, 200); //скорость движения меню в мс
        $('body').removeClass('overflow');
        $('.overlay, .city-list-popup-window').fadeOut();
    });

    $('#product-slider').slick({
        dots: false,
        autoplay: true,
        autoplaySpeed: 2500,
        infinite: true,
        speed: 300,
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1250,
                settings: {
                    arrows: false,
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1200,
                settings: {
                    arrows: true,
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1090,
                settings: {
                    arrows: false,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    arrows: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 830,
                settings: {
                    arrows: false,
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 650,
                settings: {
                    arrows: false,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 600,
                settings: {
                    arrows: true,
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

};

$(document).ready(main); //как только страница полностью загрузится, будет вызвана функция main, отвечающая за работу меню
